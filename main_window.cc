#include <phonon>
#include <QMainWindow>
#include <QPushButton>
#include <QHBoxLayout>
#include <QSignalMapper>
#include "main_window.moc"
#include <algorithm>

void MainWindow::next(int sound_type = -1)
{
    audio_output_->setMuted(true);
    if (sound_type == -1)
        sound_type = (int) ((double) qrand() / ((double) RAND_MAX / (double)(media_.size())));
    QString label = is_random_ ? "Random" : sound_buttons_[sound_type]->text();
    playing_label_->setText(label);
    for (SoundButtons::iterator sound_button_i = sound_buttons_.begin(); sound_button_i != sound_buttons_.end(); ++sound_button_i)
        (*sound_button_i)->setEnabled(is_random_);
    audio_output_ = audio_outputs_[sound_type];
    current_sound_type_ = sound_type;
    audio_output_->setMuted(false);
}

void MainWindow::playNext(int sound_type = -1)
{
    next(sound_type);
    play();
}

void MainWindow::playRandom()
{
    is_random_ = true;
    playNext();
}

void MainWindow::playChosen(int sound_type)
{
    is_random_ = false;
    playNext(sound_type);
}

void MainWindow::playChanged()
{
    int sound_type;
    do
    {
        sound_type = (int) ((double) qrand() / ((double) RAND_MAX / (double)(media_.size())));
    } while (sound_type == current_sound_type_);
    playNext(sound_type);
}

void MainWindow::resetScores()
{
    tries_ = 0;
    successes_ = 0;
    updateResult();
}

void MainWindow::mediaStateChanged(Phonon::State newstate, Phonon::State)
{
    if (newstate == Phonon::PlayingState)
        play_button_->setText("Pause");
    else
        play_button_->setText("Play");
}

/*
void MainWindow::mediaSeek(qint64 time)
{
    for (MediaObjects::iterator media_i = media_.begin(); media_i != media_.end(); ++media_i)
    {
        Phonon::MediaObject* media = *media_i;
        media->seek(time);
    }
}
*/

void MainWindow::playPause()
{
    for (MediaObjects::iterator media_i = media_.begin(); media_i != media_.end(); ++media_i)
    {
        Phonon::MediaObject* media = *media_i;
        if (media->state() == Phonon::PlayingState)
            media->pause();
        else
            media->play();
    }
}

void MainWindow::play()
{
    for (MediaObjects::iterator media_i = media_.begin(); media_i != media_.end(); ++media_i)
    {
        Phonon::MediaObject* media = *media_i;
        media->play();
    }
}

void MainWindow::picked(int sound_type)
{
    if (sound_type == current_sound_type_)
        ++successes_;
    ++tries_;
    updateResult(sound_type);
    playNext();
}

void MainWindow::updateResult(int sound_type)
{
    QString picked;
    QString real;
    if (sound_type == -1)
    {
        picked = "nothing";
        real = picked;
    }
    else
    {
        picked = sound_types_[sound_type];
        real = sound_types_[current_sound_type_];
    }

    result_label_->setText("You picked " + picked + ". It was " + real + "." + "\n" + "You tried " + QString::number(tries_) + " times and succeeded " + QString::number(successes_) + " times.");
}

struct ExtractLast
{
    QString operator()(const QString& s)
    {
        return s.split('.').last();
    }
};

MainWindow::MainWindow(const QStringList& sounds) : tries_(0), successes_(0), is_random_(true)
{
    std::transform(sounds.begin(), sounds.end(), std::back_inserter(sound_types_), ExtractLast());

    for (QStringList::const_iterator sound_i = sounds.begin(); sound_i != sounds.end(); ++sound_i)
    {
        Phonon::MediaObject* media = new Phonon::MediaObject(this);
        media->setCurrentSource(*sound_i);
        connect(media, SIGNAL(stateChanged(Phonon::State, Phonon::State)), this, SLOT(mediaStateChanged(Phonon::State, Phonon::State)));
        Phonon::AudioOutput *audio_output = new Phonon::AudioOutput(Phonon::MusicCategory, this);
        audio_output->setMuted(true);
        Phonon::createPath(media, audio_output);
        media_.push_back(media);
        audio_outputs_.push_back(audio_output);
    }
    audio_output_ = audio_outputs_[0];

    QVBoxLayout* vbox = new QVBoxLayout;

    play_button_ = new QPushButton("Play");
    connect(play_button_, SIGNAL(clicked()), this, SLOT(playPause()));
    vbox->addWidget(play_button_);

    Phonon::SeekSlider *slider = new Phonon::SeekSlider;
    slider->setMediaObject(media_[0]);
    slider->setTracking(false);
    vbox->addWidget(slider);

    QLabel* label = new QLabel("Playing:");
    vbox->addWidget(label);

    playing_label_ = new QLabel;
    vbox->addWidget(playing_label_);

    label = new QLabel("File to play:");
    vbox->addWidget(label);

    QSignalMapper* signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(playChosen(int)));
    QHBoxLayout* sound_types_layout = new QHBoxLayout;
    QWidget *sound_types_widget = new QWidget();
    vbox->addWidget(sound_types_widget);
    for (QStringList::size_type i = 0; i < sounds.size(); ++i)
    {
        QPushButton* sound_type_button = new QPushButton(sound_types_[i]);
        sound_types_layout->addWidget(sound_type_button);
        signalMapper->setMapping(sound_type_button, i);
        connect(sound_type_button, SIGNAL(clicked()), signalMapper, SLOT (map()));
    }
    QPushButton* sound_type_button = new QPushButton("Random");
    sound_types_layout->addWidget(sound_type_button);
    connect(sound_type_button, SIGNAL(clicked()), this, SLOT (playRandom()));
    sound_type_button = new QPushButton("Change");
    sound_types_layout->addWidget(sound_type_button);
    connect(sound_type_button, SIGNAL(clicked()), this, SLOT (playChanged()));
    sound_types_widget->setLayout(sound_types_layout);

    label = new QLabel("You believe this is:");
    vbox->addWidget(label);

    signalMapper = new QSignalMapper(this);
    sound_types_layout = new QHBoxLayout;
    sound_types_widget = new QWidget();
    vbox->addWidget(sound_types_widget);
    for (QStringList::size_type i = 0; i < sounds.size(); ++i)
    {
        QPushButton* sound_type_button = new QPushButton(sound_types_[i]);
        sound_buttons_.push_back(sound_type_button);
        sound_types_layout->addWidget(sound_type_button);
        signalMapper->setMapping(sound_type_button, i);
        connect(sound_type_button, SIGNAL(clicked()), signalMapper, SLOT (map()));
    }
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(picked(int)));
    sound_types_widget->setLayout(sound_types_layout);

    label = new QLabel("Test results:");
    vbox->addWidget(label);

    result_label_ = new QLabel;
    result_label_->setTextInteractionFlags(Qt::TextSelectableByKeyboard | Qt::TextSelectableByMouse);
    vbox->addWidget(result_label_);

    QPushButton* reset_button = new QPushButton("Reset results");
    vbox->addWidget(reset_button);
    connect(reset_button, SIGNAL(clicked()), this, SLOT (resetScores()));

    QWidget *main_widget = new QWidget();
    main_widget->setLayout(vbox);
    setCentralWidget(main_widget);

    updateResult();

    qsrand(time(NULL));

    next();
}
