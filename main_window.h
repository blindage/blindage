#include <phonon>
#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QVector>

class MainWindow : public QMainWindow
{
    Q_OBJECT

    typedef QVector<QPushButton*> SoundButtons;
    typedef QVector<Phonon::MediaObject*> MediaObjects;
    typedef QVector<Phonon::AudioOutput*> AudioOutputs;

    public slots:

    void mediaStateChanged(Phonon::State newstate, Phonon::State);
    void playPause();
    void picked(int sound_type);
    void playChanged();
    void playChosen(int sound_type);
    void playRandom();
    void playNext(int sound_type);
    void resetScores();
    void next(int is_random);
    void play();

    public:

    MainWindow(const QStringList&);

    private:

    void updateResult(int sound_type = -1);

    QStringList sound_types_;
    int tries_;
    int successes_;
    bool is_random_;
    QPushButton* play_button_;
    QLabel* result_label_;
    QLabel* playing_label_;
    int current_sound_type_;
    MediaObjects media_;
    SoundButtons sound_buttons_;
    AudioOutputs audio_outputs_;
    Phonon::AudioOutput* audio_output_;
    
};
