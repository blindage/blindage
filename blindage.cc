#include <QApplication>
#include "main_window.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("blindage");
    app.setQuitOnLastWindowClosed(true);

    QStringList sounds;
    for (int i = 1; i < argc; ++i)
        sounds << argv[i];

    MainWindow main_window(sounds);
    main_window.show();

    return app.exec();
}
